package Controlador;

import Modelo.cotizacion;
import Vista.dlgCotizacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class controlador implements ActionListener {

    private cotizacion cot;
    private dlgCotizacion vista;

    public controlador(cotizacion cot, dlgCotizacion vista) {
        this.cot = cot;
        this.vista = vista;
        //se inicialisan los botones del programa
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
    }
    //inicializamos la vista del programa

    private void iniciarvista() {
        vista.setTitle(":: Menu de cotizacion ::");
        vista.setSize(420, 560);
        vista.setVisible(true);
    }

    //btn nuevo
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
        if (e.getSource() == vista.btnNuevo) {
            vista.btnMostrar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.txtNumco.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.TxtPoinicial.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPlazo.setEnabled(true);
        }

        //btn de cerrar
        if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        //btn limpiar
        if (e.getSource() == vista.btnLimpiar) {
            vista.TxtPoinicial.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotfin.setText("");
            vista.txtNumco.setText("");
            vista.txtPagmen.setText("");
            vista.txtPainicial.setText("");
        }
        //btn cancelar 
        if (e.getSource() == vista.btnCancelar) {
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.txtNumco.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.TxtPoinicial.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPlazo.setEnabled(false);


            vista.TxtPoinicial.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotfin.setText("");
            vista.txtNumco.setText("");
            vista.txtPagmen.setText("");
            vista.txtPainicial.setText("");
        }

        //btn mostrar
        if (e.getSource() == vista.btnMostrar) {
            vista.txtNumco.setText(String.valueOf(cot.getNumco()));
            vista.txtDescripcion.setText(String.valueOf(cot.getDescripcion()));
            vista.txtPrecio.setText(String.valueOf(cot.getPrecio()));
            vista.TxtPoinicial.setText(String.valueOf(cot.getPorcentajepagoinicial()));
            vista.txtPlazo.getSelectedItem();
            vista.txtPainicial.setText(String.valueOf(cot.calcularpagoinicial()));
            vista.txtTotfin.setText(String.valueOf(cot.calculartotalfin()));
            vista.txtPagmen.setText(String.valueOf(cot.calcularpagomensual()));
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.txtNumco.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.TxtPoinicial.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPlazo.setEnabled(false);

        }
        //btn guardar



        if (e.getSource() == vista.btnGuardar) {
            JFrame Frame = new JFrame("Guardado con existo");
            try {

                cot.setNumco(Integer.parseInt(vista.txtNumco.getText()));
                cot.setDescripcion(vista.txtDescripcion.getText());
                cot.setPrecio(Integer.parseInt(vista.txtPrecio.getText()));
                cot.setPorcentajepagoinicial(Integer.parseInt(vista.TxtPoinicial.getText()));
                if (vista.txtPlazo.getSelectedItem().toString().length() < 8) {
                    this.cot.setPlazo(Integer.parseInt(this.vista.txtPlazo.getSelectedItem().toString().substring(0, 1)));
                } else {
                    this.cot.setPlazo(Integer.parseInt(this.vista.txtPlazo.getSelectedItem().toString().substring(0, 2)));
                }

                JOptionPane.showMessageDialog(vista, "Guardado con Exito");

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
            }

        }

    }

    public static void main(String[] args) {
        cotizacion cot = new cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);

        controlador contra = new controlador(cot, vista);
        contra.iniciarvista();
        // TODO code application logic here

    }
}
