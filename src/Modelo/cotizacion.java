package Modelo;

public class cotizacion {

    private int numco;
    private String descripcion;
    private float precio;
    private float porcentajepagoinicial;
    private int plazo;

    public int getNumco() {
        return numco;
    }

    public void setNumco(int numco) {
        this.numco = numco;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajepagoinicial() {
        return porcentajepagoinicial;
    }

    public void setPorcentajepagoinicial(float porcentajepagoinicial) {
        this.porcentajepagoinicial = porcentajepagoinicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public float calcularpagoinicial() {
        return precio * porcentajepagoinicial / 100;
    }

    public float calculartotalfin() {
        return precio - calcularpagoinicial();
    }

    public float calcularpagomensual() {
        return calculartotalfin() / plazo;
    }
}
